import { HellopercyPage } from './app.po';

describe('hellopercy App', () => {
  let page: HellopercyPage;

  beforeEach(() => {
    page = new HellopercyPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
